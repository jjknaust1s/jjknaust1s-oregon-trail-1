function Traveler(name) {
    this.name = name;
    this.food = 1;
    this.isHealthy = true;
}
Traveler.prototype = {
    constructor: Traveler,
    hunt: function() {
        this.food += +2
    },
    eat: function() {
        if (this.food === 0) {
            return this.isHealthy = false
        } else {
            this.food -= 1
        }
    }
}

// function Doctor(name) {
//     Traveler.call(this, name);
// }
// Doctor.prototype = Object.create(Traveler.prototype);
// Doctor.prototype.constructor = Doctor;
// Doctor.prototype.heal = function(patient) {
//     patient.isHealthy = true
// }



function Wagon(capacity) {
    this.capacity = capacity
    this.passengers = []
    this.food
}
Wagon.prototype = {
        constructor: Wagon,
        getAvailableSeatCount: function() {
            return this.capacity - this.passengers.length

        },
        join: function(newTraveler) {
            if (this.getAvailableSeatCount() === 0) {
                return
            }
            this.passengers.push(newTraveler)
        },
        shouldQuarantine: function() {
            for (let i = 0; i < this.passengers.length; i++) {

                if (this.passengers[i].isHealthy === false) {

                    return true
                }

            }
            return false

        },
        totalFood: function() {
            allFood = 0
            for (let i = 0; i < this.passengers.length; i++) {
                allFood += this.passengers[i].food
            }

            return allFood
        }
    }
    // Create a wagon that can hold 2 people
let wagon = new Wagon(2);
// Create three travelers
let henrietta = new Traveler('Henrietta');
let juan = new Traveler('Juan');
let maude = new Traveler('Maude');
console.log(`${wagon.getAvailableSeatCount()} should be 2`);
wagon.join(henrietta);
console.log(`${wagon.getAvailableSeatCount()} should be 1`);
wagon.join(juan);
wagon.join(maude); // There isn't room for her!
console.log(`${wagon.getAvailableSeatCount()} should be 0`);
henrietta.hunt(); // get more food
juan.eat();
juan.eat(); // juan is now hungry (sick)
console.log(`${wagon.shouldQuarantine()} should be true since juan is sick`);
console.log(`${wagon.totalFood()} should be 3`);